/*====================================
=            ON DOM READY            =
====================================*/
$(function() {
  
    // Toggle Nav on Click
    $('.toggle-nav').click(function(e) {
        e.preventDefault();
        $("#site-menu").toggleClass("show-nav");
        $(this).find('i').toggleClass('fa-close fa-bars');
        $('body').toggleClass('nav-open');
    });

  	$('.slider').owlCarousel({
        center: true,
        items:1,
        nav: true,
        loop:true,
        center: true,
        autoplay: 4500,
        slideSpeed: 1700,
        responsive: true,
        responsiveRefreshRate: 100,
        autoplayHoverPause: true,
        //stagePadding: 100,
        // animateOut: 'bounceOut',
        // animateIn: 'fadeIn',
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        responsive:{
            600:{items:1}
        }
    });

    $('#shareBtn').click(function() {
        $(".share-track").toggleClass("show");
    });

    $('label.openTracks').click(function() {
        $(".tracks-select").toggleClass("show");
    });

    
});