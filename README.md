# README #

Author: Shawn DJBigdad Brandt
Started: Feb 2 2017
Finished: Oct 10 2017

The database is not included in this build. Please reach out to Shawn Brandt aka DJBigdad if you really want the database. This published build is just for typical interviewing process.
Most traffic is via djbigdad.com/music/
Once an album or playlist has been selected included is a dynamic way to share each album.
Oneshot webapp - DJBigdad.com/oneshot - colaborate with other artist and vote/compete with others.
Contact DJBigdad.com/contact for website ideas or comments.

### What repo is this? ###

* DJBigdad.com built with Expression Engine v2.11
* This software is for promoting, playing music/albums, and allowing artist to compete via Oneshot. Both DJBigdad and colaborating artist are apart of this amazing build.
* Sell albums or collect donations.

### What is the goal of the website/webapp ###

* Allow fan to share DJBigdad music/album easily for free via streaming.
* Die hard fans will spend the extra money to get album art and the best digital copies.
* VIA djbigdad.com/Oneshot 
- Promote DJBigdad and Colaborating artist equally.
- Bring the best artist, video editors, rappers etc together.
- Friendly Voting and comeptition
- load promotional media from colaborations not included in build (only database or frontend of site)
* Deployment instructions
- being that the database isnt included this is typically just for a reviewing process.
- loaded via a localhost over XAMP
- then converted to using SFTP plugin via sublime to submit updates live. 
- packaged / backedup to bitbucket for review.
* engage social media personally to jump start the project.

### Typical Use / Guidelines ###

* Code review
* Learning / Evolution of EE

### Technical Info ###
* Twitter Bootstrap
- Jquery, Javascript
- Custom elements collected over the years
- Custom UI via Oneshot
- Owl Carousel
* Expression Engine v2.11
- Freeform Pro
- Hacksaw
- Templated Embeds - EE Organization
* Payment Gateway via Paypal

### How do I do it? ###

* Searching the opensource internet land, devotee, research/observe frontend ui/ux concepts across the web. AWWWARDS, Also colaborating with backend friends to learn more js/jQuery development. This project has been in development since Feb. 2017. Credit to Shawn DJBigdad Brandt