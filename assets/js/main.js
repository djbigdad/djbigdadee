/*====================================
=            ON DOM READY            =
====================================*/
$(function() {
  
    // Toggle Nav on Click
    $('.toggle-nav').click(function(e) {
        e.preventDefault();
        $("#site-menu").toggleClass("show-nav");
        $(this).find('i').toggleClass('fa-close fa-bars');
        $('body').toggleClass('nav-open');
    });

    $('#shareBtn').click(function() {
        $(".share-track").toggleClass("show");
    });

    $('label.openTracks').click(function() {
        $(".tracks-select").toggleClass("show");
    });

    
});