window.addEventListener( 'load', init, false );

var context;
var $body = $( 'body' );
var playing = false;
var songSource = null;
var songBuffer = null;
var startOffset = 0;
var startTime = 0;
var analyser;
var canvas = $( 'canvas' )[ 0 ];
var ctx = canvas.getContext( '2d' );
var bars = Array( 300 );
var forward = true;

// Settings
var globalHash = getHash();
var hash =  globalHash;
if ( hash.hide_controls ) {
    $( '.controls' ).addClass( 'hide' );
}
if ( hash.small ) {
    $body.addClass( 'small' );
}
console.log( hash );
var barCount = 200;
var lineWidth = hash.width || 7;
var lineGap = hash.gap || 5;
var heightFactor = hash.height || 6;
var delay = hash.delay || 27;
var animate = hash.animate || 'out';
var animateSwitch = hash.auto_delay || 5 * 1000;
var hue = hash.hue || 35;
//Put the URL into the html for Expression Engine
// var songUrl = hash.song ? "https://dl.dropboxusercontent.com/u/15727879/Childish%20Gambino%20-%20Rosenberg%20Freestyle.mp3" :
//  "https://dl.dropboxusercontent.com/u/15727879/kirby%20(co-prod.%20Isaiah%20Rashad)_244850520_soundcloud.mp3";
// var songName = hash.song || 'do_it_like';

// Preferred params
// #width=2&height=2&gap=7&delay=16&hue=0&animate=out&auto_delay=1000&song=do_it_like&hide_controls=0&small=0

// jQuery visualizr props wrappers for controls

var $out = $( '[name=animate][value=out]' );
var $in = $( '[name=animate][value=in]' );
var $auto = $( '[name=animate][value=auto]' );
var $hue = $( '[name=hue]' );
var $delay = $( '[name=delay]' );
var $width = $( '[name=width]' );
var $height = $( '[name=height]' );
var $gap = $( '[name=gap]' );
var $autoDelay = $( '[name=auto-delay]' );

function init() {
    try {
        window.AudioContext = window.AudioContext || window.webkitAudioContext;
        context = new AudioContext();

        resize();
        $( window ).on( 'resize', resize );

        flip();
        loadSong( songUrl );
    } catch ( err ) {
        console.error( 'Web Audio API is not supported in this browser' );
    }
}

function flip() {
    if ( animate == 'auto' ) {
        if ( forward ) {
            forward = false;
        } else {
            forward = true;
        }
    }
    setTimeout( flip, animateSwitch );
}

function loadSong( url ) {
    var request = new XMLHttpRequest();
    request.open( 'GET', url, true );
    request.responseType = 'arraybuffer';

    request.onload = function() {
        context.decodeAudioData( request.response, function( buffer ) {
            songBuffer = buffer;

            analyser = context.createAnalyser();
            analyser.smoothingTimeConstant = 0.3;
            analyser.fftSize = 1024;

            $body.addClass( 'loaded' );

            update();

            play();

        }, onError );
    };
    request.send();
}

function resize( evt ) {
    var $win = $( window );
    var winWidth = $win.width();
    var winHeight = $win.height();
    barCount = ( winWidth / ( lineWidth * 2 ) ) / 2;
    canvas.width = winWidth;
    canvas.height = winHeight;
}

function update() {
   // get the average, bincount is fftsize / 2
    var array =  new Uint8Array( analyser.frequencyBinCount );
    analyser.getByteFrequencyData( array );
    var average = getAverageVolume( array );
    average *= heightFactor;

    bars[ 0 ] = average;
    average *= 0.8;
    if ( playing ) {
        var reduce = 0;
        for ( var i = 1; i < barCount; i++ ) {
            average = average - Math.sqrt( average ) + 1;
            if ( average < 0 ) {
                average = 0;
            }
            (function( i, average ) {
                setTimeout( function() {
                    bars[ i ] = average;
                }, delay * ( forward ? i : 60 - i ) );
            })( i, average );
        }
    }

    draw();

    updateHash();

    requestAnimationFrame( update );
}

function draw() {
    var canvasWidth = canvas.width;
    var canvasHeight = canvas.height;

    // clear the current state
    ctx.clearRect( 0, 0, canvasWidth, canvasHeight );

    // set the fill style

    var average = bars[ 0 ];
    var color = getColor( average );
    rect( ( canvasWidth / 2 ) - ( lineWidth / 2 ), ( canvasHeight / 2 ) - ( average / 2 ), lineWidth, average, color );
    for ( var i = 1; i < barCount; i++ ) {
        average = bars[ i ];
        color = getColor( average );

        if ( average === undefined || average <= 0 ) {
            average = 0;
        } else {
            rect( ( canvasWidth / 2 ) - ( lineWidth / 2 ) + ( ( lineWidth + lineGap ) * i ), ( canvasHeight / 2 ) - ( average / 2 ), lineWidth, average, color );
            rect( ( canvasWidth / 2 ) - ( lineWidth / 2 ) - ( ( lineWidth + lineGap ) * i ), ( canvasHeight / 2 ) - ( average / 2 ), lineWidth, average, color );
        }
    }
}

var originalColors = [
    '#2B2B2B',
    '#FF5B28',
    '#2D3047',
    '#FF0032',
    '#DF928E',
    '#EAD2AC',
    '#EDEEC9',
    '#6699CC',
    '#FBFBF2',
    '#5E8860',
];
var colors = _.extend( [], originalColors );

function getColor( val ) {
    // account for hue index
    if ( hue === 0 ) {
        colors = _.extend( [], originalColors );
        for ( var i = 0; i < hue; i++ ) {
            colors.unshift( colors.pop() );
        }
        var whiteIndex = colors.indexOf( '#333' );
        colors.splice( whiteIndex, 1 );
        colors.unshift( '#333' );
    } else {
        colors = Array( 10 );
        colors[ 0 ] = '#333';
        var lightness = 49;
        for ( var i = 9; i >= 1; i-- ) {
            colors[ i ] = 'hsl(' + hue + ', 100%, ' + lightness + '%)'
            lightness -= 5;
        }
    }

    var colorIndex = Math.floor( val / ( 10 * heightFactor ) );
    if ( colorIndex > 9 ) {
        colorIndex = 9;
    } else if ( colorIndex < 0 ) {
        colorIndex = 0;
    }
    return colors[ colorIndex ];
}

function rect( x, y, width, height, color ) {
    ctx.save();
    ctx.beginPath();
    ctx.rect( x, y, width, height );
    ctx.stroke();
    ctx.clip();

    ctx.fillStyle = color;
    ctx.fillRect( 0,0,canvas.width,canvas.height );
    ctx.restore();
}

function getAverageVolume( array ) {
    var values = 0;
    var average;

    var length = array.length;

    // get all the frequency amplitudes
    for ( var i = 0; i < length; i++ ) {
        values += array[ i ];
    }

    average = values / length;
    return average;
}

function play() {
    startTime = context.currentTime;
    songSource = context.createBufferSource();
    songSource.connect( analyser );
    songSource.buffer = songBuffer;
    songSource.connect( context.destination );
    songSource.loop = isComp ? false : true;
    if (isComp) {
        songSource.onended = onAudioEnded;
    }
    songSource.start( 0, startOffset % songBuffer.duration );
    togglePlaying();
    if (!$('.comp-videos').hasClass('show')) {
        $('.showCompBtn span').text("Show Competitors");
    }
    
}

function stop() {
    songSource.stop( 0 );
    startOffset += context.currentTime - startTime;
    togglePlaying();
}

var onAudioEnded = function() {     
    $(".comp-videos").addClass("show");
    $(".compHook").removeClass("middle");
    $('.showCompBtn span').text("Hide Competitors");
}

function togglePlaying() {
    if ( playing ) {
        $body.removeClass( 'playing' );
        playing = false;
    } else {
        $body.addClass( 'playing' );
        playing = true;
    }
}

function updateHash() {
    var props = [];
    var hash = '';
    hash = 'width=' + lineWidth + '&' +
        'height=' + heightFactor + '&' +
        'gap=' + lineGap + '&' +
        'delay=' + delay + '&' +
        'hue=' + hue + '&' +
        'animate=' + animate + '&' +
        'auto_delay=' + animateSwitch + '&' +
        'song=' + songName + '&' +
        'hide_controls=' + ( globalHash.hide_controls || 0 ) + '&' +
        'small=' + ( globalHash.small || 0 );

    if ( window.location.hash != hash ) {
        window.location.hash = hash;
    }
}

function getHash() {
    return window.location.hash
    .replace( /^\#/, '' )
    .split( '&' )
    .reduce( function( memo, keyVal ) {
        var key = keyVal.split( '=' )[ 0 ];
        var val = keyVal.split( '=' )[ 1 ];

        if ( key != 'animate' && key != 'song' ) {
            val = parseInt( val );
        }

        memo[ key ] = val;
        return memo;
    }, {} );
}

function onError( err ) {
    console.error( err );
}

$( '.playpause' ).on( 'click', function() {
    if ( playing ) {
        stop();
    } else {
        play();
        
    }
});

$out.on( 'click', function( evt ) {
    if ( evt.currentTarget.checked ) {
        forward = true;
        animate = 'out';
    }
});

$in.on( 'click', function( evt ) {
    if ( evt.currentTarget.checked ) {
        forward = false;
        animate = 'in';
    }
});

$auto.on( 'click', function( evt ) {
    if ( evt.currentTarget.checked ) {
        animate = 'auto';
    }
});

$delay.on( 'input', function() {
    var val = $delay.val();
    // console.log( val * 1.2 );
    delay = Math.floor( val * 1.2 );
});

$width.on( 'input', function() {
    var winWidth = $( window ).width();
    barCount = ( winWidth / ( lineWidth + lineGap ) ) / 2;
    lineWidth = 1 + Math.floor( ( $width.val() / 2 ) );
});

$gap.on( 'input', function() {
    lineGap = Math.floor( ( $gap.val() / 2.5 ) );
});

$height.on( 'input', function() {
    heightFactor = 1 + ( $height.val() / 10 );
});

$autoDelay.on( 'input', function() {
    animateSwitch = Math.floor( $autoDelay.val() / 10 ) * 1000;
});

$hue.on( 'input', function() {
    // hue = Math.floor( $hue.val() / 10 );
    hue = Math.floor( ( 361 * ( $hue.val() / 100 ) ) );
});

$("#menuToggle, #close").on('click', function() {
    $(".menu").toggleClass('open');
    $("#close").toggleClass('show');
});

$("#menuToggle").on('click', function() {
    $(this).toggleClass('byebye');
});

$("#close").on('click', function() {
    $('#menuToggle').toggleClass('byebye');
});

$("#toggleDetails").on('click', function() {
    $("body.loaded .track-details-wrap").toggleClass('put-away');
    $(".control-wrap").toggleClass('move');
});

$("#controlSettings").on('click', function() {
    $(".toggleBlock").removeClass('on');
    $("#playlistContent").addClass('toggleBlock');
    $("#controlsSettingsContent").toggleClass('on');
});

$("#controls").on('click', function() {
    $(".toggleBlock").removeClass('on');
    $("#playlistContent").toggleClass('toggleBlock');
});

$(".menu-control-btn").on('click', function() {
    $(".menu-control-btn").removeClass('active');
    $(this).addClass('active');
});


$( function() {
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
    $('#sortable').draggable();
});

// Attempt to stop music when a video is played
// $(".comp-videos.show iframe").on('click', function(e) {
//     e.preventDefault();
//     $("button.playpause").trigger();
// });

$( function() {
    $('.compHook').click(function(){      
        if ( playing ) {
            stop();
        } else {
            play();
            
        }
    });
});

// Attempt to trigger button when audio is done.
// var audio1 = document.getElementById("whereIsTheAudioElement?");
// audio1.onended = function() {
//     alert("audio playback has ended");
//     $( "#nextSong" ).trigger();
// };
